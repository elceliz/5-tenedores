import { createStackNavigator } from 'react-navigation-stack'
import SearchRestaurantsScreen from '../screens/Search'

const SearchRestaurantsScreenStacks = createStackNavigator({
  TopList : {
    screen : SearchRestaurantsScreen,
    navigationOptions : () => ({
      title : "Buscar restaurantes"
    })
  }
})

export default SearchRestaurantsScreenStacks
