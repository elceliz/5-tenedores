import { createStackNavigator } from 'react-navigation-stack'
import TopRestaurantsScreen from '../screens/TopRestaurants'

const TopRestaurantsScreenStacks = createStackNavigator({
  TopList : {
    screen : TopRestaurantsScreen,
    navigationOptions : () => ({
      title : "Los mejores restaurantes"
    })
  }
})

export default TopRestaurantsScreenStacks
