import React from 'react'
import { Icon } from 'react-native-elements'
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'

import RestaurantScreenStacks from './RestaurantsStacks'
import TopListScreenStacks from './TopListStacks'
import SearchScreenStacks from './SearchStacks'
import AccountScreenStacks from './AccountStacks'

const NavigationStacks = createBottomTabNavigator(
{
  Restaurants : {
    screen : RestaurantScreenStacks,
    navigationOptions : () => ({
      tabBarLabel : "Restaurantes",
      tabBarIcon : ({ tintColor }) => (
        <Icon
          type="material-community"
          name="compass-outline"
          size={22}
          color={tintColor}
        />
      )
    })
  },
  TopList : {
    screen : TopListScreenStacks,
    navigationOptions : () => ({
      tabBarLabel : "Ranking",
      tabBarIcon : ({ tintColor }) => (
        <Icon
          type="material-community"
          name="compass-outline"
          size={22}
          color={tintColor}
        />
      )
    })
  },
  Search : {
    screen : SearchScreenStacks,
    navigationOptions : () => ({
      tabBarLabel : "Search",
      tabBarIcon : ({ tintColor }) => (
        <Icon
          type="material-community"
          name="compass-outline"
          size={22}
          color={tintColor}
        />
      )
    })
  },
  MyAccount : {
    screen : AccountScreenStacks,
    navigationOptions : () => ({
      tabBarLabel : "Mi Cuenta",
      tabBarIcon : ({ tintColor }) => (
        <Icon
          type="material-community"
          name="compass-outline"
          size={22}
          color={tintColor}
        />
      )
    })
  },
},
{
  initialRouteName : 'Restaurants',
  order : ["Restaurants","TopList","Search","MyAccount"],
  tabBarOptions : {
    inactiveTintColor : "#646464",
    activeTintColor : "#00a680"
  }
}
)

export default createAppContainer(NavigationStacks)
