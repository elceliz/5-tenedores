import firebase from 'firebase/app'

const firebaseConfig = {
  apiKey: "AIzaSyCg51xIeV40DZKNZJXELqIhn0iPVTI3iLY",
  authDomain: "tenedores-8b89a.firebaseapp.com",
  databaseURL: "https://tenedores-8b89a.firebaseio.com",
  projectId: "tenedores-8b89a",
  storageBucket: "tenedores-8b89a.appspot.com",
  messagingSenderId: "284344987198",
  appId: "1:284344987198:web:bac149d531ea57f5b2ab6f"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);

// <!-- The core Firebase JS SDK is always required and must be listed first -->
// <script src="https://www.gstatic.com/firebasejs/7.5.0/firebase-app.js"></script>
//
// <!-- TODO: Add SDKs for Firebase products that you want to use
//      https://firebase.google.com/docs/web/setup#available-libraries -->
//
// <script>
//   // Your web app's Firebase configuration
//   var firebaseConfig = {
//     apiKey: "AIzaSyCg51xIeV40DZKNZJXELqIhn0iPVTI3iLY",
//     authDomain: "tenedores-8b89a.firebaseapp.com",
//     databaseURL: "https://tenedores-8b89a.firebaseio.com",
//     projectId: "tenedores-8b89a",
//     storageBucket: "tenedores-8b89a.appspot.com",
//     messagingSenderId: "284344987198",
//     appId: "1:284344987198:web:bac149d531ea57f5b2ab6f"
//   };
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);
// </script>
