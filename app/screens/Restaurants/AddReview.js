import React, { useState, useRef } from 'react'
import { StyleSheet, View } from 'react-native'
import { AirbnbRating, Button, Input } from 'react-native-elements'
import Toast from 'react-native-easy-toast'

import { firebaseApp } from '../../utils/Firebase'
import firebase from 'firebase/app'
import 'firebase/firestore'

const db = firebase.firestore(firebaseApp)

export default function AddReview(props) {
    
    const { navigation } = props
    const { idRestaurant } = navigation.state.params
    
    const [title, setTitle] = useState("")
    const [review, setReview] = useState("")
    const [rating, setRating] = useState(null)

    const toast = useRef()

    const addReview = () => {
        if(rating === null) {
            toast.current.show('No has elegido ninguna puntuación')
            return
        }
        if(!title) {
            toast.current.show('Debes escribir un título')
            return
        }
        if(!review) {
            toast.current.show('Debes escribir un comentario')
            return
        }

        const user = firebase.auth().currentUser

        const payload = {
            idUser : user.uid,
            avatarUser : user.photoURL,
            idRestaurant : idRestaurant,
            title : title,
            review : review,
            rating : rating,
            createAt : new Date()
        }
        

        db.collection('reviews').add(payload).then(() => {
            updateRestaurant()
        })
        .catch(e => toast.current.show('Ocurrió un error : ' + e))
    }

    const updateRestaurant = () => {
    
        const restaurantRef = db.collection('restaurants').doc(idRestaurant)
    
        restaurantRef.get().then(response => {
    
            const restaurantData = response.data()
            const ratingTotal = restaurantData.ratingTotal + rating;
            const quantityVoting = restaurantData.quantityVoting + 1;
            const ratingResult = ratingTotal / quantityVoting
    
            restaurantRef.update({ rating : ratingResult, ratingTotal, quantityVoting }).then(() => {
                navigation.goBack()
            })
            .catch(e => console.error(e))
        })
    }

    return (
        <View style={styles.viewBody}>
            <View style={styles.viewRating}>
                <AirbnbRating
                    count={5}
                    reviews={["Pésimo","Deficiente","Normal","Muy Bueno","Excelente"]}
                    defaultRating={0}
                    size={35}
                    onFinishRating={value => setRating(value)}
                />
            </View>
            <View style={styles.formReview}>
                <Input
                    placeholder="Titulo"
                    containerStyle={styles.input}
                    onChange={e => setTitle(e.nativeEvent.text)}
                />
                <Input
                    placeholder="Comentario"
                    multiline={true}
                    inputContainerStyle={styles.textArea}
                    onChange={e => setReview(e.nativeEvent.text)}
                />
                <Button 
                    title="Enviar Comentario" 
                    onPress={addReview} 
                    containerStyle={styles.btnContainer} 
                    buttonStyle={styles.btn}
                />
            </View>
            <Toast ref={toast} position="center" opacity={0.5} />
        </View>
    )
}

const styles = StyleSheet.create({
    viewBody : {
        flex : 1
    },
    viewRating : {
        height : 110,
        backgroundColor : '#f2f2f2'
    },
    formReview : {
        margin : 10,
        flex : 1,
        alignItems : 'center',
        marginTop : 40
    },
    input : {
        marginBottom : 10
    },
    textArea : {
        height : 150,
        width : '100%',
        padding : 0,
        margin : 0
    },
    btnContainer : {
        flex : 1,
        justifyContent : 'flex-end',
        marginTop : 20,
        marginBottom : 10,
        width : '95%'
    },
    btn : {
        backgroundColor : '#00a680'
    }
})
