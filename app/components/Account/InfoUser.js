import React from 'react';
import { StyleSheet, View, Text } from 'react-native'
import { Avatar } from 'react-native-elements'
import * as firebase from 'firebase'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'

export default function InfoUser(props) {

  const {
    userInfo : {
      uid,
      displayName,
      email,
      photoURL,
      providerId,
    },
    setReloadData,
    setIsVisibleLoading,
    setTextLoading,
    toastRef
  } = props

  const changeAvatar = async () => {
    const resultPermissions = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    const resultPermissionCamera = resultPermissions.permissions.cameraRoll.status

    if(resultPermissionCamera === "denied") {
      toastRef.current.show('Es necesario aceptar los permisos de la galeria')
    }
    else {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowEditing : true,
        aspect : [4, 3]
      })

      if(result.cancelled) {
        toastRef.current.show('Has cerrado la galeria sin seleccionar ninguna imagenes')
      }
      else {
        uploadImage(result.uri, uid).then(response => {
          toastRef.current.show('Imagen correctamente subida')
          updatePhotoUrl(uid)
        })
      }
    }
  }

  const uploadImage = async (uri, imageName) => {

    setTextLoading('Actualizando Avatar')
    setIsVisibleLoading(true)

    const response = await fetch(uri)
    const blob = await response.blob()
    const ref = firebase.storage().ref().child(`avatar/${imageName}`)

    return ref.put(blob)
  }

  const updatePhotoUrl = uid => {
    firebase.storage()
      .ref(`avatar/${uid}`)
      .getDownloadURL()
      .then(async result => {

        const update = {
          photoURL : result
        }
        await firebase.auth().currentUser.updateProfile(update)
        setReloadData(true)
        setIsVisibleLoading(false)
      })
      .catch(response => toastRef.current.show('error al recuperar el avatar del servidor' + response))
  }

  return (
    <View style={styles.viewUserInfo}>
      <Avatar
        rounded
        size="large"
        showEditButton={providerId === 'facebook.com' ? false : true}
        onEditPress={changeAvatar}
        containerStyle={styles.userInfoAvatar}
        source={{
          uri : photoURL ? photoURL : "https://api.adorable.io/avatars/263/abott@adorable.png"
        }}
      />
      <View style={styles.displayName}>
        <Text>{displayName ? displayName : 'Anonimo'}</Text>
        <Text>{email}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  viewUserInfo : {
    alignItems : 'center',
    justifyContent : 'center',
    flexDirection : 'row',
    backgroundColor : '#f2f2f2',
    paddingTop : 30,
    paddingBottom : 30
  },
  userInfoAvatar : {
    marginRight : 20
  },
  displayName : {

  }
})
