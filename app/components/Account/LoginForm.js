import React from 'react';
import { StyleSheet, View } from 'react-native'
import { Input, Button, Icon } from 'react-native-elements'
import { validateEmail } from '../../utils/Validation'
import Loading from '../Loading'
import * as firebase from 'firebase'

export default function LoginForm(props) {

  const { toastRef, navigation } = props

  const [email, setEmail] = React.useState("")
  const [password, setPassword] = React.useState("")
  const [hidePassword, setHidePassword] = React.useState(true)
  const [isVisibleLoading, setIsVisibleLoading] = React.useState(false)

  const login = async () => {

    if(!email || !password) {
      toastRef.current.show('Todos los campos son obligatarios')
      return
    }

    if(!validateEmail(email)) {
      toastRef.current.show('el email no es correcto')
      return
    }

    setIsVisibleLoading(true)

    await firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(response => navigation.navigate('MyAccount'))
    .catch(response => toastRef.current.show('Email o contraseña incorrecta'))

    setIsVisibleLoading(false)
  }

  return (
    <View style={styles.formContainer}>
      <Input
        placeholder="Correo electrónico"
        containerStyle={styles.inputForm}
        onChange={e => setEmail(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name="at"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Contraseña"
        password={true}
        secureTextEntry={hidePassword}
        containerStyle={styles.inputForm}
        onChange={e => setPassword(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hidePassword ? "eye-outline" : "eye-off-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setHidePassword(!hidePassword)}
          />
        }
      />
      <Button
        title="Iniciar sesión"
        containerStyle={styles.btnContainerRegister}
        buttonStyle={styles.btnRegister}
        onPress={login}
      />
    <Loading text="Iniciando sesión..." isVisible={isVisibleLoading} />
    </View>
  )
}

const styles = StyleSheet.create({
  formContainer : {
    flex : 1,
    alignItems : 'center',
    justifyContent : 'center',
    marginTop : 30
  },
  inputForm : {
    width : '100%',
    marginTop : 20,
  },
  iconRight : {
    color : '#c1c1c1'
  },
  btnContainerRegister : {
    marginTop : 20,
    width : '95%'
  },
  btnRegister : {
    backgroundColor : '#00a680',
  }
})
