import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import { Input, Button } from 'react-native-elements'
import * as firebase from 'firebase'
import { validateEmail } from '../../utils/Validation'
import { reauthenticate } from '../../utils/Api'

export default function ChangeEmailForm(props) {

    const { email, setIsVisibleModal, setReloadData, toastRef } = props

    const [newEmail, setNewEmail] = useState("")
    const [password, setPassword] = useState("")
    const [hidePassword, setHidePassowrd] = useState(true)
    const [error, setError] = useState({})
    const [isLoading, setIsLoading] = useState(false)

    const updateEmail = () => {
        setError(null)
        if(!newEmail || email === newEmail) {
            setError({ email : 'El email es el mismo o esta vacío' })
        }
        else{
            if(!validateEmail(newEmail)) {
                setError({ email : 'El email no es válido '})
            }
            else {
                setIsLoading(true)
                reauthenticate(password).then(response => {
                    const update = {
                        email : newEmail
                    }
                    firebase.auth().currentUser.updateEmail(newEmail)
                    .then(response => {
                        setIsLoading(false)
                        setReloadData(true)
                        toastRef.current.show('Email actualizado correctamente')
                        setIsVisibleModal(false)
                    })
                    .catch(error => {
                        toastRef.current.show('Error al actualizar el email : ' + error)
                        setIsLoading(false)
                    })
                })
                .catch(error => {
                    toastRef.current.show('Ocurrió un error al autenticar : ' + error)
                    setIsVisibleModal(false)
                    setIsLoading(false)
                })
            }
        }
    }

    return (
        <View style={styles.view}>
        <Input
            placeholder="Email"
            containerStyle={styles.input}
            defaultValue={email || ""}
            onChange={e => setNewEmail(e.nativeEvent.text)}
            rightIcon={{
                type : "material-community",
                name : "at",
                color : "#c2c2c2"
            }}
            errorMessage={error && error.email}
        />
        <Input
            placeholder="Contraseña"
            password={true}
            secureTextEntry={hidePassword}
            containerStyle={styles.input}
            onChange={e => setPassword(e.nativeEvent.text)}
            rightIcon={{
                type : "material-community",
                name : hidePassword ? "eye-outline" : "eye-off-outline",
                color : "#c2c2c2",
                onPress : () => setHidePassowrd(!password)
            }}
            errorMessage={error && error.password}
        />
        <Button 
            title="Cambiar email"
            containerStyle={styles.btnContainer}
            buttonStyle={styles.btn}
            onPress={updateEmail}
            loading={isLoading}
        />
    </View>
    )
}

const styles = StyleSheet.create({
    view : {
        alignItems : 'center',
        paddingTop : 10,
        paddingBottom : 10
    },
    input : {
        marginBottom : 10,
    },
    btnContainer : {
        marginTop : 20,
        width : "95%"
    },
    btn : {
        backgroundColor : "#00a680"
    }
})