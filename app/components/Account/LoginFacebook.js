import React, { useState } from 'react';
import * as Facebook  from 'expo-facebook'
import * as firebase from 'firebase'
import { SocialIcon } from 'react-native-elements'
import { FacebookApi } from '../../utils/Social'
import Loading from '../Loading'

export default function LoginFacebook(props) {

  const { toastRef, navigation } = props

  const [isVisibleLoading, setIsVisibleLoading] = React.useState(false)

  const login = async () => {

    setIsVisibleLoading(true)

    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
      FacebookApi.application_id,
      { permissions : FacebookApi.permissions }
    )

    if(type === "success") {
      const credentials = firebase.auth.FacebookAuthProvider.credential(token)

      await firebase
      .auth()
      .signInWithCredential(credentials)
      .then(response => navigation.navigate("MyAccount"))
      .catch(response => toastRef.current.show('error accediendo con facebook : ' + response))
    }

    if(type === "cancel") {
      console.info('inicio de sesion cancelado...')
    }


    setIsVisibleLoading(false)

  }

  return (
    <>
      <SocialIcon
        title="Iniciar sesión con Facebook"
        type="facebook"
        button
        onPress={login}
      />
      <Loading text="Iniciando sesión..." isVisible={isVisibleLoading} />
    </>
  )
}
