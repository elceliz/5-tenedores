import React, { useState } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Input, Button } from 'react-native-elements'
import * as firebase from 'firebase'
import { reauthenticate } from '../../utils/Api'


export default function ChangePasswordForm(props) {

    const { setIsVisibleModal, setReloadData, toastRef } = props

    const [password, setPassword] = useState("")
    const [newPassword, setNewPassword] = useState("")
    const [newPasswordRepeated, setNewPasswordRepeated] = useState("")
    const [error, setError] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [hidePassword, setHidePassword] = useState(true)
    const [hideNewPassword, setHideNewPassword] = useState(true)
    const [hideNewPasswordRepeated, setHideNewPasswordRepeated] = useState(true)

    const updatePassword = () => {
        setError(null)
        if(!password || !newPassword || !newPasswordRepeated) {

            const objError = {}

            if(!password) objError.password = 'El password no puede estar vacío'

            if(!newPassword) objError.newPassword = 'El nuevo password no puede estar vacío'

            if(!newPasswordRepeated) objError.newPasswordRepeated = 'El nuevo password no puede estar vacío'
        
            setError(objError)

            return
        
        }

        if(newPassword !== newPasswordRepeated) {
            setError({
                newPassword : 'Las nuevas contraseñas deben ser iguales',
                newPasswordRepeated : 'Las nuevas contraseñas deben ser iguales'
            })

            return
        }

        setIsLoading(true)
        reauthenticate(password).then(response => {
            firebase.auth().currentUser.updatePassword(newPassword)
            .then(response => {
                setIsLoading(false)
                toastRef.current.show('Contraseña actualizada')
                setIsVisibleModal(false)
                firebase.auth().signOut()
            })
            .catch(error => setError({ general : "Error al actualizar la contraseña "}))
        })
        .catch(error => {
            setError({ password : 'No pudo autenticarse : ' + error })
            setIsLoading(false)
        })

    }

    return (
        <View style={styles.view}>
            <Input
                placeholder="Contraseña actual"
                containerStyle={styles.input}
                password={true}
                secureTextEntry={hidePassword}
                onChange={e => setPassword(e.nativeEvent.text)}
                rightIcon={{
                    type : "material-community",
                    name : hidePassword ? "eye-outline" : "eye-off-outline",
                    color : "#c2c2c2",
                    onPress : () => setHidePassword(!hidePassword)
                }}
                errorMessage={error && error.password}
            />
            <Input
                placeholder="Nueva contraseña"
                containerStyle={styles.input}
                password={true}
                secureTextEntry={hideNewPassword}
                onChange={e => setNewPassword(e.nativeEvent.text)}
                rightIcon={{
                    type : "material-community",
                    name : hideNewPassword ? "eye-outline" : "eye-off-outline",
                    color : "#c2c2c2",
                    onPress : () => setHideNewPassword(!hideNewPassword)
                }}
                errorMessage={error && error.newPassword}
            />
            <Input
                placeholder="Repetir contraseña"
                containerStyle={styles.input}
                password={true}
                secureTextEntry={hideNewPasswordRepeated}
                onChange={e => setNewPasswordRepeated(e.nativeEvent.text)}
                rightIcon={{
                    type : "material-community",
                    name : hideNewPasswordRepeated ? "eye-outline" : "eye-off-outline",
                    color : "#c2c2c2",
                    onPress : () => setHideNewPasswordRepeated(!hideNewPasswordRepeated)
                }}
                errorMessage={error && error.newPasswordRepeated}
            />
            <Button 
                title="Cambiar contraseña"
                containerStyle={styles.btnContainer}
                buttonStyle={styles.btn}
                onPress={updatePassword}
                loading={isLoading}
            />
            <Text>{error && error.general}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    view : {
        alignItems : 'center',
        paddingTop : 10,
        paddingBottom : 10
    },
    input : {
        marginBottom : 10,
    },
    btnContainer : {
        marginTop : 20,
        width : "95%"
    },
    btn : {
        backgroundColor : "#00a680"
    }
})