import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, FlatList } from 'react-native'
import { Button, Avatar, Rating } from 'react-native-elements'

export default function ListReview(props) {

    const { navigation, idRestaurant } = props

    return (
        <View>
            <Button
                buttonStyle={styles.buttonAddReview}
                titleStyle={styles.titleAddReview}
                title="Escribir una opinión"
                icon={{
                    type : 'material-community',
                    name : 'square-edit-outline',
                    color : '#00a680'
                }}
                onPress={() => navigation.navigate('AddReview', { idRestaurant : idRestaurant })}
            />
            <Text>Lista de comentarios...</Text>
        </View>
    )

}

const styles = StyleSheet.create({
    buttonAddReview : {
        backgroundColor : "transparent"
    },
    titleAddReview : {
        color : "#00a680"
    }
})